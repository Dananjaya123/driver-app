import React, { Component } from "react";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import Animated from "react-native-reanimated";
import { StyleSheet, Text, Image, View } from "react-native";
import { firebase } from "../database/firebaseDb";

export default class HomeScreen extends Component {
  constructor() {
    super();
    let user = firebase.auth().currentUser
    this.firestoreRef = firebase.firestore().collection("children").where("driver_email", '==', user.email);
    this.state = {
      isLoading: true,
      userArr: [],
    };
  }

  componentDidMount() {
    this.unsubscribe = this.firestoreRef.onSnapshot(this.getCollection);
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  getCollection = (querySnapshot) => {
    const userArr = [];
    querySnapshot.forEach((res) => {
      const { image, firstName, lastName, school, vehicle_number } = res.data();
      userArr.push({
        key: res.id,
        res,
        image,
        firstName,
        lastName,
        school,
        vehicle_number,
      });
    });
    this.setState({
      userArr,
      isLoading: false,
    });
  };

  render() {
    return (
      <View>
        <ScrollView>
          {this.state.userArr.map((item, i) => {
            return (
              <TouchableOpacity style={styles.card}>
                <Animated.View style={[{ flexDirection: "row" }]}>
                  <View style={styles.cardImage}>
                    <Image
                      style={{ width: 80, height: 80, borderRadius: 20 }}
                      source={{ uri: item.image }}
                    />
                  </View>
                  <View
                    style={{
                      flex: 0.6,
                      marginHorizontal: 12,
                      overflow: "hidden",
                    }}
                  >
                    <Text style={styles.cardTitle}>
                      {item.firstName} {item.lastName}
                    </Text>
                    <Text style={styles.cardSchool}>{item.school}</Text>
                    <Text style={styles.cardSchool}>{item.vehicle_number}</Text>
                  </View>
                </Animated.View>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#000",
    padding: 12,
    paddingVertical: 8,
    paddingHorizontal: 15,
    marginVertical: 10,
    marginHorizontal: 20,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.3,
    shadowRadius: 1.5,
  },
  cardTitle: {
    fontWeight: "bold",
    fontSize: 16,
    marginLeft: 20,
  },
  cardSchool: {
    fontSize: 12,
    color: "#777",
    marginLeft: 20,
  },
  cardImage: {
    padding: 0,
    flex: 0.3,
  },
});
