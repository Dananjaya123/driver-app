import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome5";
import * as Animatable from "react-native-animatable";
import { StatusBar } from "expo-status-bar";

import {firebase} from '../database/firebaseDb'


export default function SignUpScreen({navigation}) {

  const [userName, setUserName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  const onSuccessLinkPress = () => {
    navigation.navigate('SignInScreen')
  }

  const onRegisterPress = () => {
    if (password !== confirmPassword) {
      alert("Password don't match.")
      return
    }
    firebase.auth()
    .createUserWithEmailAndPassword(email,password)
    .then((response) => {
      const uid = response.user.uid
      const data = {
        id: uid,
        email,
        userName
      };
      const usersRef = firebase.firestore().collection('drivers')
      usersRef.doc(uid)
      .set(data)
      .then(() => {
        navigation.navigate('HomeScreen', {user:data})
      })
      .catch((error) => {
        alert(error)
      });
    })
    .catch((error) => {
      alert(error)
    })
  }
  return (
    <View style={styles.container}>
    <StatusBar backgroundColor="#344955" style="light" />
    <View style={styles.header}>
      <Text style={styles.text_header}>Register Now</Text>
    </View>
    <Animatable.View animation="fadeInUpBig" style={styles.footer}>
      <Text style={styles.text_footer}>User Name</Text>
      <View style={styles.action}>
        <FontAwesome name="user-alt" color="#0537a" size={20} />
        <TextInput
          placeholder="Your User Name"
          style={styles.textInput}
          autoCapitalize="none"
          value={userName}
          onChangeText={(text) => setUserName(text)}
        />
      </View>
      <Text style={[styles.text_footer, { marginTop: 25 }]}>Email</Text>
      <View style={styles.action}>
        <FontAwesome name="envelope" color="#0537a" size={20} />
        <TextInput
          placeholder="Yor Email"
          style={styles.textInput}
          autoCapitalize="none"
          keyboardType="email-address"
          value={email}
          onChangeText={(text) => setEmail(text)}
        />
      </View>
      <Text style={[styles.text_footer, { marginTop: 25 }]}>Password</Text>
      <View style={styles.action}>
        <FontAwesome name="unlock-alt" color="#0537a" size={20} />
        <TextInput
          placeholder="Your Password"
          secureTextEntry={true}
          style={styles.textInput}
          autoCapitalize="none"
          value={password}
          onChangeText={(text) => setPassword(text)}
          maxLength={15}
        />
        <TouchableOpacity
        >
          </TouchableOpacity>
      </View>
      <Text style={[styles.text_footer, 
            {marginTop: 25}]}>Confirm Password</Text>
            <View style={styles.action}>
                <FontAwesome
                name="unlock-alt"
                color="#0537a"
                size={20}/>
                <TextInput 
                placeholder="Confirm Your Password"
                secureTextEntry={true}
                style={styles.textInput}
                autoCapitalize="none"
                value={confirmPassword}
                onChangeText={(text) => setConfirmPassword(text)}
                />
                <TouchableOpacity
                >
                </TouchableOpacity>
            </View>

      <TouchableOpacity
        style={styles.button1}
        onPress={() => onRegisterPress()}
      >
        <View>
          <Text style={styles.buttonText}>Sign Up</Text>
        </View>
      </TouchableOpacity>
      <View>
            <Text
                style={{ fontWeight: '200', fontSize: 17, textAlign: 'center', marginTop: 35}}
                onPress={onSuccessLinkPress}
            >
                Already have an account? SignIn
            </Text>
            </View>
    </Animatable.View>
  </View>

  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#344955",
  },
  header: {
    flex: 1,
    justifyContent: "flex-end",
    paddingHorizontal: 20,
    paddingBottom: 40,
  },
  footer: {
    flex: 6,
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 30,
  },
  text_footer: {
    color: "#05375a",
    fontSize: 18,
  },
  action: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#f2f2f2",
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    // marginTop: Platform.OS === 'android' ? 0 : -12,
    paddingLeft: 10,
    color: "#05375a",
  },
  button1: {
    alignItems: "center",
    marginTop: 30,
    backgroundColor: "#F9AA33",
    width: "100%",
    height: 50,
    justifyContent: "center",
    elevation: 5,
  },
  button2: {
    alignSelf: "center",
    alignItems: "center",
    marginTop: 30,
    backgroundColor: "#F9AA33",
    width: "30%",
    height: 50,
    justifyContent: "center",
    elevation: 5,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: "bold",
  },
  signIn: {
    width: "100%",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: "bold",
  },
  signintext1: {
    alignSelf: "center",
    marginTop: 32,
    fontSize: 16,
    fontWeight: "bold",
    flexDirection: "row",
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff",
  },
});
