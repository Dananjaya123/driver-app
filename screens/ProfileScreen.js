import React, { Component } from 'react'
import {
  View,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  ActivityIndicator,
  Image,
} from "react-native";
import { Avatar, Title, Caption, Text } from "react-native-paper";
import Icon from "react-native-vector-icons/FontAwesome5";
import {firebase} from '../database/firebaseDb';



export default class ProfileScreen extends Component {
  constructor() {
    super();
    let user = firebase.auth().currentUser
    this.firestoreRef = firebase.firestore().collection('drivers').where("id", "==", user.uid);
    this.state = {
      isLoading: true,
      userArr: []
    };
  }

  componentDidMount() {
    this.unsubscribe = this.firestoreRef.onSnapshot(this.getCollection);
  }

  componentWillUnmount(){
    this.unsubscribe();
  }

  getCollection = (querySnapshot) => {
    const userArr = [];
    querySnapshot.forEach((res) => {
      const { image, userName, email, mobile, nic, address, vehicle_number } = res.data();
      userArr.push({
        key: res.id,
        res,
        image,
        userName,
        email,
        mobile,
        nic,
        address,
        vehicle_number
      });
    });
    this.setState({
      userArr,
      isLoading: false,
   });
  }

  clickHandler = () => {
     this.props.navigation.navigate('EditProfileScreen');
  };

  
  render() {
      
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }

    return (
     <>
      <SafeAreaView style={styles.container}>
      {
        this.state.userArr.map((item, i) => {
      return (
      <View style={styles.userInfoSection}>
        <View style={{ flexDirection: "row", marginTop: 15 }}>
          <Avatar.Image
            source={{
              uri: item.image
            }}
            size={80}
          />
          <View style={{ marginLeft: 20 }}>
            <Title style={[styles.title, { marginTop: 15, marginBottom: 5 }]}>
              {item.userName}
            </Title>
            <Caption style={styles.caption}>{item.email}</Caption>
          </View>
        </View>
      </View>
      )
    })
    }
    {
      this.state.userArr.map((item, i) => {
      return (
      <View style={styles.userInfoSection}>
        <View style={styles.row}>
          <Icon name="map-marker-alt" color="#777777" size={15} />
          <Text style={{ color: "#777777", marginLeft: 20 }}>
            {item.address}
          </Text>
        </View>
        <View style={styles.row}>
          <Icon name="phone" color="#777777" size={15} />
          <Text style={{ color: "#777777", marginLeft: 20 }}>{item.mobile}</Text>
        </View>
        <View style={styles.row}>
          <Icon name="id-badge" color="#777777" size={20} />
          <Text style={{ color: "#777777", marginLeft: 20 }}>{item.nic}</Text>
        </View>
        <View style={styles.row}>
          <Icon name="shuttle-van" color="#777777" size={20} />
          <Text style={{ color: "#777777", marginLeft: 20 }}>{item.vehicle_number}</Text>
        </View>
      </View>
      )
    })
    }
            <TouchableOpacity
          activeOpacity={0.7}
          onPress={this.clickHandler}
          style={styles.TouchableOpacityStyle}>
          <Image
            source={{
              uri:
                'https://www.pngrepo.com/png/92412/180/pen.png',
            }}
            style={styles.FloatingButtonStyle}
          />
        </TouchableOpacity>
    </SafeAreaView>
    </>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
    marginTop: 50,
  },
  userInfoSection: {
    paddingHorizontal: 30,
    marginBottom: 55,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: "500",
  },
  row: {
    flexDirection: "row",
    marginBottom: 30,
  },
  infoBoxWrapper: {
    borderBottomColor: "#dddddd",
    borderBottomWidth: 1,
    borderTopColor: "#dddddd",
    borderTopWidth: 1,
    flexDirection: "row",
    height: 60,
  },
  infoBox: {
    width: "50%",
    alignItems: "center",
    justifyContent: "center",
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: "row",
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: "#777777",
    marginLeft: 20,
    fontWeight: "600",
    fontSize: 16,
    lineHeight: 26,
  },
  inputGroup: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  },
  MainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5F5F5',
  },
  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
  },
  FloatingButtonStyle: {
    resizeMode: 'contain',
    width: 50,
    height: 50,
    //backgroundColor:'black'
  },
});

